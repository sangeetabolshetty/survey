var app = new Vue({
    el: '#app',
    data: {
      survey: [],
      questionIndex: 0,
      innerIndex: 0,
      isShowing: false,
      isHidden: false,
      statementIsTrue: 'Yes',
      checkedNames: [],
      indexing: 1
    },
    mounted: function () {
      axios.get('survay-questions.json')
        .then(response => {
          this.survey = response.data;
        })
        .catch(error => {
          console.log(error);
        });
    },
    methods: {
      show: function () {
        this.isShowing = true;
        if (this.questionIndex == -1) {
          this.questionIndex++;      
        }
      },
      // Go to next question
      next: function (index) {
        this.innerIndex++;
        this.indexing++;
        
        if(index != this.questionIndex) {
          this.questionIndex++;
          this.innerIndex=0;
        }
        
      },
      // Go to previous question
      prev: function () {
        this.questionIndex--;
        this.indexing--;
        if (this.indexing == 1 && this.questionIndex == -1) {
          this.questionIndex++;
          this.innerIndex++;
        } 

        if(this.questionIndex == -1) {
          this.isShowing = false;
        }else {
          this.isShowing = true;
        }
        
      }    
    }
  })